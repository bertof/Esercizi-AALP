\chapter{Subtyping}

\section*{Sintassi}
\begin{align*}
    Termini\ M & ::= x \mid n \mid true\ \mid false                                      \\
               & \ \ \mid\  M+M \mid M-M \mid \sif{M}{M}{M}                              \\
               & \ \ \mid \sfn{x:T}{M} \mid M\ M \mid \sset{l_i=M\ind{i}{1}{n}} \mid M.l \\
    Valori\ v  & ::= n \mid true \mid false\ \sfn{x:T}{M} \mid \sset{l_i=v\ind{i}{1}{n}} \\
    Tipi\ T    & ::= Nat \mid Bool \mid T\rightarrow T \mid \sset{l_i:T\ind{i}{1}{n}}
\end{align*}

\section*{Regole di tipo}
La relazione di subtyping formalizza l’intuizione che alcuni tipi sono più informativi di altri: diciamo che \(S\) è sottotipo di \(T\), scritto \(S<:T\), per indicare che
\begin{itemize}
    \item i valori di tipo \(S\) sono un sottoinsieme dei valori di tipo \(T\) (subset semantics), cioè ogni valore di tipo \(S\) è anche un valore di tipo \(T\);
    \item un termine di tipo \(S\) può essere usato correttamente in un contesto che si aspetta un termine di tipo \(T\) (Principio di sostituzione).
\end{itemize}
\begin{prooftree}
    \AXC{\(\Gamma\vdash M:S\)}
    \AXC{\(S<:T\)}
    \LLB{(SUBSUMPTION)}
    \BIC{\(\Gamma\vdash M:T\)}
\end{prooftree}

\section*{Relazione di sottotipo}
La relazione di sottotipo è formalizzata tramite un insieme di regole di inferenza che derivano giudizi della forma \(S<:T\).
Oltre alle regole specifiche per i vari costruttori di tipo, questo insieme contiene due regole che definiscono la riflessività e la transitività della relazione di subtyping, in accordo con l’intuizione data dal principio di sostituzione.

\begin{align*}
    \AXC{}
    \LLB{(REFLEX)}
    \UIC{\(T<:T\)}
    \DP &  &
    \AXC{\(S<:U\)}
    \AXC{\(U<:T\)}
    \LLB{(TRANS)}
    \BIC{\(S<:T\)}
    \DP
\end{align*}

Un linguaggio con tipi base può includere degli assiomi che definiscono una relazione di subtyping tra tipi base.
Ad esempio, assiomi di subtyping per i tipi base potrebbero essere \(Nat<:Int\) oppure \(Nat<:Bool\).

\subsection*{Subtyping per i tipi record}
\begin{align*}
    \AXC{}
    \LLB{(SUB WIDTH)}
    \UIC{\(\sset{l_i:T\ind{i}{1}{n+k}}<:\sset{l_i:T}\ind{i}{1}{n}\)}
    \DP &  &
    \AXC{\(\sforall{i\in\srange{1}{n}}S_i<:T_i\)}
    \LLB{(SUB DEPTH)}
    \UIC{\(\sset{l_i:S\ind{i}{1}{n}}<:\sset{l_i:T}\ind{i}{1}{n}\)}
    \DP
\end{align*}
\begin{prooftree}
    \AXC{\(\sset{l\ind{i}{1}{n}}\text{\ è una permutazione di\ }\sset{k\ind{j}{1}{m}}\)}
    \LLB{(PERMUTE)}
    \UIC{\(\sset{l\ind{i}{1}{n}}<:\sset{k\ind{j}{1}{m}}\)}
\end{prooftree}

Le tre regole di subtyping per i tipi record si possono riassumere in una singola:

\begin{prooftree}
    \AXC{\(\sset{l\ind{i}{1}{n}}\subseteq\sset{k\ind{j}{1}{m}}\)}
    \AXC{\(k_j=l_i\implies S_j<:T_i\)}
    \LLB{(SUB REC)}
    \BIC{\(\sset{k_j:S\ind{j}{1}{m}}<:\sset{l_i:T\ind{i}{1}{n}}\)}
\end{prooftree}

\section{Esercizio - Scrivere le derivazioni dei giudizi}
\begin{itemize}
    \item \(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat},l':\sset{}}\)
    \item \(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat},l':\sset{m:Nat}}\)
    \item \(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat}}\)
\end{itemize}

\subsection*{Soluzione 1}
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{a:Nat,b:Nat}<:\sset{a:Nat}\)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{m:Nat}<:\sset{}\)}
    \RLB{\nameref{rule:t_sub_depth}}
    \BIC{\(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat},l':\sset{}}\)}
\end{prooftree}

\subsection*{Soluzione 2}
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{a:Nat,b:Nat}<:\sset{a:Nat}\)}
    \AXC{}
    \RLB{\nameref{rule:t_reflex}}
    \UIC{\(\sset{m:Nat}<:\sset{m:Nat}\)}
    \RLB{\nameref{rule:t_sub_depth}}
    \BIC{\(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat},l':\sset{m:Nat}}\)}
\end{prooftree}

\subsection*{Soluzione 3}
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{a:Nat,b:Nat}<:\sset{a:Nat}\)}
    \AXC{}
    \RLB{\nameref{rule:t_reflex}}
    \UIC{\(\sset{m:Nat}<:\sset{m:Nat}\)}
    \RLB{\nameref{rule:t_sub_depth}}
    \BIC{\(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat},l':\sset{m:Nat}}\)}
    \AXC{(1)}
    \RLB{\nameref{rule:t_trans}}
    \BIC{\(\sset{l:\sset{a:Nat,b:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat}}\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{l:\sset{a:Nat},l':\sset{m:Nat}}<:\sset{l:\sset{a:Nat}}\)}
\end{prooftree}

\section{Esercizio - Derivazione di tipo}
Si scriva la derivazione di \(\sset{a:Nat,b:Bool,c:Nat}<:\sset{b:Bool}\)
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_permute}}
    \UIC{\(\sset{a:Nat,b:Bool,c:Nat}<:\sset{b:Bool,a:Nat,c:Nat}\)}
    \AXC{(1)}
    \RLB{\nameref{rule:t_trans}}
    \BIC{\(\sset{a:Nat,b:Bool,c:Nat}<:\sset{b:Bool}\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{b:Bool,a:Nat,c:Nat}<:\sset{b:Bool}\)}
\end{prooftree}

\section*{Subtyping per i tipi funzione}
\begin{prooftree}
    \AXC{\(T_1<:S_1\)}
    \AXC{\(S_2<:T_2\)}
    \LLB{(ARROW)}
    \BIC{\(S_1\rightarrow S_2<:T_1\rightarrow T_2\)}
\end{prooftree}

\section{Esercizio - Derivazione di giudizio}
Dare la derivazione del giudizio \(\emptyset\vdash(\sfn{r:\sset{l:Nat}}{r.l}+2)\ \sset{l=0,l'=1}:Nat\).
Esiste una sola derivazione per questo giudizio?

\subsection*{Soluzione}
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_var}}
    \UIC{\(r:\sset{l:Nat}\vdash r:\sset{l:Nat}\)}
    \RLB{\nameref{rule:t_select}}
    \UIC{\(r:\sset{l:Nat}\vdash r.l:Nat\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(r:\sset{l:Nat}\vdash 2:Nat\)}
    \RLB{\nameref{rule:t_sum}}
    \BIC{\(r:\sset{l:Nat}\vdash r.l+2:Nat\)}
    \RLB{\nameref{rule:t_fun}}
    \UIC{\(\emptyset\vdash\sfn{r:\sset{l:Nat}}{r.l+2}:\sset{l:Nat}\rightarrow Nat\)}
    \AXC{(1)}
    \RLB{\nameref{rule:t_app}}
    \BIC{\(\emptyset\vdash(\sfn{r:\sset{l:Nat}}{r.l}+2)\ \sset{l=0,l'=1}:Nat\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\emptyset\vdash\sset{l=0,l'=1}:\sset{l:Nat,l':Nat}<:\sset{l:Nat}\)}
    \RLB{\nameref{rule:t_subsumption}}
    \UIC{\(\emptyset\vdash\sset{l=0,l'=1}:\sset{l:Nat}\)}
    \RLB{\nameref{rule:t_app}}
\end{prooftree}

Un'altra possibile soluzione è quella di applicare subsumption al tipo della funzione, per poi applicare le regole di subtyping per funzioni.

\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_var}}
    \UIC{\(r:\sset{l:Nat}\vdash r:\sset{l:Nat}\)}
    \RLB{\nameref{rule:t_select}}
    \UIC{\(r:\sset{l:Nat}\vdash r.l:Nat\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(r:\sset{l:Nat}\vdash 2:Nat\)}
    \RLB{\nameref{rule:t_sum}}
    \BIC{\(r:\sset{l:Nat}\vdash r.l+2:Nat\)}
    \RLB{\nameref{rule:t_fun}}
    \UIC{\(\emptyset\vdash\sfn{r:\sset{l:Nat}}{r.l+2}:\sset{l:Nat}\rightarrow Nat\)}
    \AXC{(2)}
    \RLB{\nameref{rule:t_subsumption}}
    \BIC{\(\emptyset\vdash\sfn{r:\sset{l:Nat}}{r.l+2}:\sset{l:Nat,l':Nat}\rightarrow Nat\)}
    \AXC{(1)}
    \RLB{\nameref{rule:t_app}}
    \BIC{\(\emptyset\vdash(\sfn{r:\sset{l:Nat}}{r.l}+2)\ \sset{l=0,l'=1}:Nat\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 0:Nat\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 1:Nat\)}
    \RLB{\nameref{rule:t_record}}
    \BIC{\(\emptyset\vdash\sset{l=0,l'=1}:\sset{l:Nat,l':Nat}\)}
\end{prooftree}
\begin{prooftree}{(2)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{l:Nat,l':Nat}<:\sset{l:Nat}\)}
    \AXC{}
    \RLB{\nameref{rule:t_reflex}}
    \UIC{\(Nat<:Nat\)}
    \RLB{\nameref{rule:t_arrow}}
    \BIC{\(\sset{l:Nat}\rightarrow Nat<:\sset{l:Nat,l':Nat}\rightarrow Nat\)}
\end{prooftree}

\section{Esercizio - Dire se il termine è ben tipato}
Dire se il termine \(\sif{true}{\sset{a=1,b=0}}{\sset{a=3,c=1}}\) è ben tipato nel linguaggio con subtyping.

\subsection*{Soluzione}
%TODO da ricontrollare la subsumption
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_true}}
    \UIC{\(\emptyset\vdash true:Bool\)}
    \AXC{(1)}
    \AXC{(2)}
    \TIC{\(\emptyset\vdash\sif{true}{\sset{a=1,b=0}}{\sset{a=3,c=1}}:T\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 3:T_{1a}\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 1:T_{1c}\)}
    \BIC{\(\emptyset\vdash \sset{a=3,c=1}:T_1\)}
    \AXC{\(T_1<:T\)}
    \RLB{\nameref{rule:t_subsumption}}
    \BIC{\(\emptyset\vdash \sset{a=3,c=1}:T\)}
\end{prooftree}
\begin{prooftree}{(2)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 1:T_{2a}\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 0:T_{2b}\)}
    \BIC{\(\emptyset\vdash \sset{a=1,b=0}:T_2\)}
    \AXC{\(T_2<:T\)}
    \RLB{\nameref{rule:t_subsumption}}
    \BIC{\(\emptyset\vdash \sset{a=1,b=0}:T\)}
\end{prooftree}
\[
    T_1=\sset{a:Nat,c:Nat}<:T,T_2=\sset{a:Nat,c:Nat}\implies T=\sset{a:Nat}
\]
\begin{align*}
    T      & = \sset{a:Nat}       \\
    T_1    & = \sset{a:Nat,c:Nat} \\
    T_2    & = \sset{a:Nat,c:Nat} \\
    T_{1a} & = Nat                \\
    T_{1c} & = Nat                \\
    T_{2a} & = Nat                \\
    T_{2b} & = Nat                \\
\end{align*}

\section{Esercizio - Giudizio sul tipo}
Dire se il termine \(\sif{true}{1}{false}\) è ben tipato nel linguaggio con subtyping.

\subsection*{Soluzione}
Provo a derivare il giudizio di tipo \(\emptyset\vdash\sif{true}{1}{false}:T\).
\begin{prooftree}
    \AXC{}
    \RLB{\nameref{rule:t_true}}
    \UIC{\(\emptyset\vdash true:Bool\)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash1:T\)}
    \AXC{}
    \RLB{\nameref{rule:t_false}}
    \UIC{\(\emptyset\vdash false:T\)}
    \RLB{\nameref{rule:t_if_then_else}}
    \TIC{\(\emptyset\vdash\sif{true}{1}{false}:T\)}
\end{prooftree}
Ma questo indica \(T=Bool\) e \(T=Nat\) ma non vale né \(Bool<:Nat\) né \(Nat<:Bool\), dunque \(M\) non è ben tipato.

\section{Esercizio - Relazione di sottotipo per i variant}
Quale potrebbe essere la relazione di sottotipo dei variant types?

\subsection*{Soluzione}
\begin{prooftree}
    \AXC{}
    \LLB{(S-VARIANT WIDTH)}
    \UIC{\(\stup{l_i:T\ind{i}{1}{n}}<:\stup{l_i:T\ind{i}{1}{n+k}}\)}
\end{prooftree}
\begin{prooftree}
    \AXC{\(\sforall{i\in\srange{1}{n}}S_i<:T_i\)}
    \LLB{(S-VARIANT DEPTH)}
    \UIC{\(\stup{l_i:S\ind{i}{1}{n}}<:\stup{l_i:T\ind{i}{1}{n}}\)}
\end{prooftree}
\begin{prooftree}
    \AXC{\(\stup{k_j:S\ind{j}{1}{n}} \text{ è permutazione di } \stup{l_i:T\ind{i}{1}{n}}\)}
    \LLB{(S-VARIANT PERMUTE)}
    \UIC{\(\stup{k_j:S\ind{j}{1}{n}}<:\stup{l:T\ind{i}{1}{n}}\)}
\end{prooftree}
Le regole di subtyping per i tipi Variant funzionano in modo simile a quelle per i record, ma al contrario di queste, S-VARIANT WIDTH pone che un variant più ampio \((n+k)\) sia supertipo di un variant più stretto \((n\) poiché aumentando le possibili etichette dell'i-esimo tipo sta, in realtà, diminuendo l'informazione su di esso.

\section{Esercizio - Ragionamenti sul sistema di subtyping}
Quali proprietà del sistema di tipi si perderebbero se avessimo definito la relazione di subtyping con una regola di troppo?
E se l'avessimo definita con una regola di meno?
Se avessimo definito le regole in modo tale che \(\sset{l:Nat}<:\sset{l:Nat,l':Nat}\), quale proprietà del sistema non sarebbe più vera? Identificarlo e dare un controesempio.

\subsection*{Soluzione 1}
Aggiungere una regola significa che il sistema tipa per più programmi, ciò porterebbe ad avere programmi ben tipati, ma che danno errori a runtime.

\subsection*{Soluzione 2}
Togliendo regole si tipano meno programmi, escludendone di quelli che non darebbero errori:

\paragraph{Togliendo \nameref{rule:t_sub_width}}
Non posso usare record con più campi di quelli richiesti, altrimenti viene scartato dal type-system;

\paragraph{Togliendo \nameref{rule:t_sub_depth}}
Non posso usare record con un campo che è sottotipi di quello richiesto, altrimenti viene scartato dal type-system;

\paragraph{Togliendo \nameref{rule:t_permute}}
Non posso usare record con gli stessi campi di quelli richiesti ma con ordine diverso (permutazione);

\paragraph{Togliendo \nameref{rule:t_subsumption}}
Non posso usare un sottotipo dove ci si aspetta un supertipo, quindi crolla la proprietà fondamentale dei sottotipi in un type-system.

\paragraph{Togliendo \nameref{rule:t_arrow}}
Non si possono usare sottotipi o supertipi come argomenti di funzione, altrimenti verrebbero scartati dal type-system.

\subsection*{Soluzione 3}
Se \(\sset{l:Nat}<:\sset{l:Nat,l':Nat}\) allora non sarebbe più vero che un programma ben tipato non produce più errori, poiché ci sarebbero programmi che richiedono il campo \(l'\) di un termine di tipo \(\sset{l:Nat}\) e questo non è definito, mentre il type-system convaliderebbe tale programma.

\section{Lemmi di inversione con subtyping}\label{def:lemma_inversione_sub}
\begin{enumerate}
    \item \(\Gamma\vdash x:T \implies \sexists{S}x:S,\ S<:T\)
    \item \(\Gamma\vdash\sfn{x:S_1}{M:T}\text{ derivabile}\implies\sexists{T_1,T_2}T=T_1\rightarrow T_2,\ T_1<:S_1,\ \Gamma,x:S_1\vdash M:T_2\)
    \item \(\Gamma\vdash\sset{l_i=M\ind{i}{1}{n}}:T\text{ derivabile}\implies\sexists{\srange{T_1}{T_m}}T=\sset{K_j:T\ind{j}{1}{m}},\ \sforall{j\in\srange{1}{m}}\sexists{i\in\srange{1}{n}}k_j=l_i,\ \Gamma\vdash M_i:T_j\) cioè le etichette \(\srange{k_1}{k_m}\) sono un sottoinsieme delle etichette \(\srange{l_1}{l_n}\) con possibile permutazione.
\end{enumerate}

\section{Lemma di inversione della relazione di subtyping}\label{def:inv_relazione_sub}
\begin{enumerate}
    \item \(S<:T_1\rightarrow T_2\implies S=S_1\rightarrow S_2,\ T_1<:S_1,\ S_2<:T_2\)
    \item \(S<:\sset{l_i:T\ind{i}{1}{n}}\implies S=\sset{k_j:S\ind{j}{1}{m}},\ \sforall{i\in\srange{1}{n}}\sexists{j\in\srange{1}{,}}l_i=K_j,\ S_j<:T_i\).
\end{enumerate}

\section{Substitution}\label{def:substitution_sub}
\(\Gamma,x:S\vdash M:T,\ \Gamma\vdash N:S\implies \Gamma\vdash M\sset{x:=N}:T\)

\section{Preservazione dei tipi}\label{def:teorema_preservazione_sub}
\(\Gamma\vdash M:T,\ M\rightarrow M'\implies\Gamma\vdash M':T\).

\section{Forme canoniche}\label{def:lemma_forme_canoniche_sub}
\begin{itemize}
    \item \(v\text{ valore}:T_1\rightarrow T_2\implies v=\sfn{x:S_1}{M}\);
    \item \(v\text{ valore}:\sset{l_i:T\ind{i}{1}{n}}\implies v=\sset{k_j=v\ind{j}{1}{m}},\ \sset{l\ind{i}{1}{n}}\subseteq\sset{k\ind{j}{1}{m}}\).
\end{itemize}

\section{Progressione}\label{def:teorema_progressione_sub}
\(fv(M)=\emptyset,\emptyset\vdash M:T\implies M\not\rightarrow\dot\lor\sexists{M'}M\rightarrow M'\)

\section{Esercizio - Dimostrazione preservazione, progressione e safety con subtyping}
Dimostrare i teoremi di preservazione, progressione e safety per il linguaggio di base (cioè quello senza record definito nella sezione \eqref{sec:linguaggio_funzionale}) \nameref{sec:linguaggio_funzionale} esteso con subtyping.
Evidenziare come cambia la dimostrazione a causa della presenza del subtyping.

\subsection*{Soluzione}
Perché il teorema valga devo ridimostrare i teoremi di preservazione (subject reduction) e progressione.

\subsubsection*{\nameref{lemma:inversione} con subtyping}\label{lemma:inversione_sub}
\(S_1\rightarrow S_2<:T_1\rightarrow T_2\implies T_1<:S_1,S_2<:T_2\)

\paragraph*{Dimostrazione \nameref{lemma:inversione} con subtyping}\label{dim:lemma:inversione_sub}
Il \nameref{lemma:inversione} non cambia rispetto a quello per il linguaggio originale \eqref{lemma:inversione}.

\subsubsection*{\nameref{lemma:forme_canoniche} con subtyping}\label{lemma:forme_canoniche_sub}
\(fv(v)=\emptyset,\Gamma\vdash v:T_1\rightarrow T_2\implies v=\sfn{x:T_1}M_2\)

\subsubsection*{\nameref{lemma:substitution} con subtyping}\label{lemma:substitution_sub}
% TODO controllare che sia corretto
% \(\Gamma,x:S\vdash M:T,\Gamma\vdash N:S\implies\Gamma\vdash M\sset{x:=N}:T\)
Non ci sono casi aggiuntivi rispetto alla \compref{dim:lemma:substitution}.

\subsubsection*{\nameref{dim:th:progressione} con subtyping}\label{dim:th:progressione_sub}
Devo dimostrare che \(fv(M)=\emptyset,\Gamma\vdash M:T\implies M\not\rightarrow\dot\lor\sexists{M'}M\rightarrow M'\).
Eseguo la dimostrazione per induzione sull'altezza del giudizio di tipo \(\emptyset\vdash M:T\).

\paragraph{Casi base: \(h=1\)}

\subparagraph{Caso \nameref{rule:t_true}}
Immediato poiché \(M=true\) allora \(M:Bool\) per il \compref{lemma:inversione} ed è un valore finale.

La soluzione è analoga per i casi \nameref{rule:t_fun}, \nameref{rule:t_false} e \nameref{rule:t_nat}.

\paragraph{Passo induttivo: \(h+1\)}
L'ipotesi induttiva è che il teorema sia valido per i termini con derivazione di altezza al più \(k\).
Dimostro la validità del teorema per i termini con derivazione di altezza \(k+1\).

\subparagraph{Caso \nameref{rule:t_app}}
In questo caso \(M=M_1\ M_2\) e il giudizio in ipotesi è \(\emptyset\vdash M_1\ M_2:T\).
Applicando il \compref{lemma:inversione_sub} al termine \(M\) trovo che \(\Gamma\vdash M_1\ M_2:T\timplies{\nameref{rule:t_app}}\sexists{T_1}\Gamma\vdash M_1:T_1\rightarrow T,M_2:T_1\).
Applicando l'ipotesi induttiva sui sottotermini \(M_1\) e \(M_2\) distinguiamo i seguenti tre casi:
\begin{itemize}
    \item \(M_1\rightarrow M_1'\), allora l'unica regola applicabile è \nameref{rule:app_1}, con cui otteniamo \(M_1\ M_2\trightarrow{\nameref{rule:app_1}}M_1'\ M_2\).
    \item \(M_1=v_1\) valore e \(M_2\rightarrow M'\), allora posso applicare la regola \nameref{rule:app_2}, con cui otteniamo \(v_1\ M_2\rightarrow v_1\ M_2'\).
    \item \(M_1=v_1\) valore e \(M_2=v_2\) valore, allora per il lemma delle forme canoniche \(v_1=\sfn{x:S_1}{M_3}\) e l'unica regola applicabile è \nameref{rule:beta}, che produce \(\sfn{x:S_1}{M_2}\ v_2\timplies{\nameref{rule:beta}}M_3\sset{x:=v_2}\).
\end{itemize}

\subparagraph{Caso \nameref{rule:t_subsumption}}
\(\Gamma\vdash_k M:S\) e \(S<:T\), allora per la regola \nameref{rule:t_subsumption} è immediatamente derivabile tramite l'ipotesi induttiva, ovvero \(\Gamma\vdash M:T\).

La dimostrazione è uguale al linguaggio senza il subtyping \eqref{dim:th:progressione} per gli altri casi.

\subsubsection*{\nameref{dim:th:preservazione} con subtyping}\label{dim:th:preservazione_sub}
Devo dimostrare che \(\Gamma\vdash M:T,M:\rightarrow M'\implies\Gamma\vdash M':T\).
Eseguo la dimostrazione per induzione sull'altezza della giudizio di tipo \(\Gamma\vdash M:T\).

\paragraph{Casi base: \(h=1\)}
I casi \nameref{rule:t_true}, \nameref{rule:t_false}, \nameref{rule:t_fun} poiché i valori finali non procedono, dunque le premesse sono false e l'implicazione vacuamente verificata.
\subparagraph{Caso \nameref{rule:t_var}}
Il caso \nameref{rule:t_var} è anch'esso vacuamente verificato poiché \(\Gamma=\emptyset\) per ipotesi del teorema di preservazione e non ci sono regola per la valutazione di una variabile.

\paragraph{Passo induttivo: \(h=k+1\)}
Suppongo vero il teorema per i giudizi di tipo con derivazione di altezza al più \(k\) e lo mostro per i giudizi di tipo con derivazione di altezza \(k+1\).

\subparagraph{Caso \nameref{rule:t_app}}
In questo caso \(M=M_1\ M_2\) e il giudizio in ipotesi è \(\Gamma\vdash M=M_1\ M_2:T\).
Applicando il \compref{lemma:inversione_sub} al termine \(M\) trovo che \(\Gamma\vdash M_1\ M_2:T\timplies{\nameref{rule:t_app}}\sexists{T_1}M_1:T_1\rightarrow T,M_2:T\).
Applicando l'ipotesi induttiva sui sottotermini \(M_1\) e \(M_2\) distinguiamo i seguenti tre casi:
\begin{itemize}
    \item \nameref{rule:app_1}, allora \(M_1\ M_2\rightarrow M_1'\ M_2\) con premessa \(M_1\rightarrow M_1'\).
    Applico l'ipotesi induttiva su \(M_1\) trovando \(\Gamma\vdash M_1:T_1\rightarrow T,M_1\rightarrow M_1'\timplies{Hp. ind.}\Gamma\vdash M_1':T_1\rightarrow T\).
    Applicando la regola \nameref{rule:t_app} derivo che \(\Gamma\vdash M_1':T_1\rightarrow T,M_2:T_1\timplies{\nameref{rule:t_app}}\Gamma\vdash M_1'\ M_2:T\).
    \item \nameref{rule:app_2}, allora \(v_1\ M_2\rightarrow v_1\ M_2'\) con premessa \(M_2\rightarrow M_2'\).
    Applico l'ipotesi induttiva su \(M_2\) trovando \(\Gamma\vdash M_2:T_1,M_2\rightarrow M_2'\timplies{Hp. ind.}\Gamma\vdash M_2':T_1\).
    Applicando la regola \nameref{rule:t_app} derivo che \(\Gamma\vdash v_1:T_1\rightarrow T,M_2':T_1\timplies{\nameref{rule:t_app}}\Gamma\vdash v_1\ M_2':T\).
    \item \nameref{rule:beta}, allora per il \compref{lemma:forme_canoniche_sub} troviamo che \(\Gamma\vdash v_1:T_1\rightarrow T\timplies{L. f. can.}v_1=\sfn{x:S_1}{M_3},T_1<:S_1\).
    Applicando la regola \nameref{rule:t_arrow} trovo che \(T_1\rightarrow T<:S_1\rightarrow S\timplies{\nameref{rule:t_arrow}}T_1<:S_1,S<:T\).
    Applicando il \compref{def:substitution_sub} trovo che \(\Gamma,x:S\vdash M_3:T\timplies{L. sostituzione}\Gamma,x:S\vdash M_3\sset{x:=v}:T\), dunque la tesi è verificata.
\end{itemize}

\subparagraph{Caso \nameref{rule:t_if_then_else}}
Il caso \nameref{rule:t_if_then_else} è analogo a quello della \compref{dim:th:preservazione}.

\subparagraph{Caso \nameref{rule:t_sum}}
Il caso \nameref{rule:t_sum} è analogo a quello della della \compref{dim:th:preservazione}.

\subparagraph{Caso \nameref{rule:t_minus}}
Il caso \nameref{rule:t_minus} è analogo a quello della della \compref{dim:th:preservazione}.

\subparagraph{Caso \nameref{rule:t_subsumption}}
In questo caso \(M\) è un termine qualsiasi e il giudizio in ipotesi è \(\Gamma\vdash M:T\).
Poiché l'ultima regola applicata è \nameref{rule:t_subsumption} sono vere le premesse \(\Gamma\vdash M:S\) e \(S<:T\).
Applicando l'ipotesi induttiva troviamo che \(\Gamma\vdash M:S,M\rightarrow M'\timplies{Hp. ind.}\Gamma\vdash M':S\).
Applicando la regola \nameref{rule:t_subsumption} troviamo che \(\Gamma\vdash M':S,S<:T\timplies{\nameref{rule:t_subsumption}}\Gamma\vdash M':T\), dunque la tesi è verificata.

\paragraph{\nameref{dim:th:safety} con subtyping}\label{dim:th:safety_sub}
Poiché il \nameref{th:progressione} e il \nameref{th:preservazione} sono validi, allora anche il \nameref{th:safety} è valido.

\section{Esercizio - Termine non stuck con subtyping}
Definire un termine \(M\) che non evolve in un termine stuck e che compila solo nel linguaggio esteso con subtyping.
\subsection*{Soluzione}
Posso prendere come esempio il termine \(M=\sfn{x:\sset{l:Nat}}{x.l}\ \sset{l=1,l'=true}\).

\begin{prooftree}
    \AXC{(1)}
    \AXC{}
    \RLB{\nameref{rule:t_nat}}
    \UIC{\(\emptyset\vdash 1:T_{1lk}=Nat\)}
    \AXC{}
    \RLB{\nameref{rule:t_true}}
    \UIC{\(\emptyset\vdash true:T_{1lk'}=Bool\)}
    \BIC{\(\emptyset\vdash\sset{k=1,k'=true}:T_{1l}=\sset{k=Nat,k'=Bool}\)}
    \RLB{\nameref{rule:t_record}}
    \UIC{\(\emptyset\vdash\sset{l=\sset{k=1,k'=true}}:T_1=\sset{l=\sset{k=Nat,k'=Bool}}\)}
    \RLB{\nameref{rule:t_app}}
    \BIC{\(\emptyset\vdash\sfn{x:\sset{l:\sset{k:Nat}}}{x.l.k}\ \sset{l=\sset{k=1,k'=true}}:T\)}
\end{prooftree}
\begin{prooftree}{(1)}
    \AXC{\(S_1=S_x=\sset{l:\sset{k:Nat}}\)}
    \RLB{\nameref{rule:t_var}}
    \UIC{\(x:\sset{l:\sset{k:Nat}}=S_1\vdash x:S_x\)}
    \RLB{\nameref{rule:t_select}}
    \UIC{\(x:\sset{l:\sset{k:Nat}}=S_1\vdash x.l:S_l=\sset{k:Nat}\)}
    \RLB{\nameref{rule:t_select}}
    \UIC{\(x:\sset{l:\sset{k:Nat}}=S_1\vdash x.l.k:S=Nat\)}
    \RLB{\nameref{rule:t_fun}}
    \UIC{\(\emptyset\vdash\sfn{x:\sset{l:\sset{k:Nat}}}{x.l.k}:S_1\rightarrow S\)}
    \AXC{(2)}
    \RLB{\nameref{rule:t_subsumption}}
    \BIC{\(\emptyset\vdash\sfn{x:\sset{l:\sset{k:Nat}}}{x.l.k}:T_1\rightarrow T\)}
\end{prooftree}
\begin{prooftree}{(2)}
    \AXC{}
    \RLB{\nameref{rule:t_sub_width}}
    \UIC{\(\sset{k=Nat,k'=Bool}<:\sset{k:Nat}\)}
    \RLB{\nameref{rule:t_sub_depth}}
    \UIC{\(\sset{l=\sset{k=Nat,k'=Bool}}<:\sset{l:\sset{k:Nat}}\)}
    \AXC{}
    \RLB{\nameref{rule:t_reflex}}
    \UIC{\(Nat<:T\implies T=Nat\)}
    \RLB{\nameref{rule:t_arrow}}
    \BIC{\(S_1=\sset{l:\sset{k:Nat}}\rightarrow S=Nat <: T_1=\sset{l=\sset{k=Nat,k'=Bool}}\rightarrow T\)}
\end{prooftree}

\section{Correttezza e completezza del subtyping algoritmico}
\[S<:T \iff S<::T\]
\begin{multicols}{2}
    \begin{prooftree}
        \AXC{\(\sforall{i\in\srange{1}{n}}\Gamma\Vdash M_i:T_i\)}
        \LLB{(ALGO RECORD)}
        \UIC{\(\Gamma\Vdash\sset{l_i=M\ind{i}{1}{n}}:\sset{l_i:T_i}\)}
    \end{prooftree}

    \begin{prooftree}
        \AXC{\(\Gamma\Vdash M:\sset{l_i:T_i}\)}
        \LLB{(ALGO PROJECT)}
        \UIC{\(\Gamma\Vdash M.l_i:T_i\)}
    \end{prooftree}

    \begin{prooftree}
        \AXC{\(\Gamma\Vdash M:T_1\rightarrow T_2,\ N:T_1'\)}
        \AXC{\(T_1'<::T_1\)}
        \LLB{(ALGO APP)}
        \BIC{\(\Gamma\Vdash M\ N:T_2\)}
    \end{prooftree}

    \begin{prooftree}
        \AXC{\(x:T\in\Gamma\)}
        \LLB{(ALGO VAR)}
        \UIC{\(\Gamma\Vdash x:T\)}
    \end{prooftree}

    \begin{prooftree}
        \AXC{\(\Gamma,x:T_1\Vdash M:T_2\)}
        \LLB{(ALGO FUN)}
        \UIC{\(\Gamma\Vdash\sfn{x:T_1}{M}:T_1\rightarrow T_2\)}
    \end{prooftree}
\end{multicols}

\section{Correttezza}
\(\Gamma\Vdash M:T\implies\Gamma\vdash M:T\) si dimostra per induzione sulla derivazione del giudizio \(\Gamma\vdash M:T\).

\section{Completezza o Tipi Minimi}
\(\Gamma\vdash M:T\implies\sexists{S<:T}\Gamma\Vdash M:S\) si dimostra per induzione sulla derivazione del giudizio \(\Gamma\vdash M:T\).

\section{Esercizio - Termini decrescenti}
Trovare due termini \(M\) e \(N\) tali che \(M\rightarrow N,\ \Gamma\Vdash M:T,\ N:S,\ S<:T, T\not<::S\).
Cioè esibire un caso in cui il tipo di un termine decresce durante la computazione.

\subsection*{Soluzione}
Posso considerare una funzione che ritorna il valore nullo di tipo Bot:
\[M=\sfn{x:Nat}{Nul:Bot}\implies M:Nat\rightarrow Bot\]
\begin{align*}
    Nat \not<:: Bot &  & Bot<::Nat
\end{align*}